/*
 * Задача:
 * 1) Исправить ошибки в скрипте
 * 2) Оптимизировать его приведя в "красивый" вид
 * 3) Сократить потребление оперативной памяти
 * 4) Сократить время исполнения скрипта
 * 5) Посчитать комиссию, которую получит каждый бейкер с блока 832543 по 832546
 */

const { get } = require('axios')

const start = new Date().getTime()
const time = () => (new Date().getTime() - start)/1000

// Выделил загрузку в отдельную функцию на случай, если придётся менять URI.
const loadBlock = async blockNumber => {
  return (await get(`https://teznode.letzbake.com/chains/main/blocks/${blockNumber}`)).data
}

(async function() {
  const blocks = []
  const bakers_fees = {}

  // Заполняем блоки.
  for (let block = 832543; block <= 832546; block++) {
    blocks.push(await loadBlock(block))
  }

  // Обходим блоки. Тут я подразумеваю, что один блок = один бейкер.
  for (const block of blocks) {
    // Декларируем значение по умолчанию.
    bakers_fees[block.metadata.baker] ??= 0

    bakers_fees[block.metadata.baker] =
      // Прибавляем прошлое число.
      bakers_fees[block.metadata.baker] +
      // Считаем сумму комиссий по всем транзакциям.
      block
        .operations.flat(2)
        .filter(op => Array.isArray(op.contents) && op.contents.length)
        .map(op => op.contents).flat()
        .filter(content => content.kind === 'transaction')
        .reduce((acc, row) => acc + parseFloat(row.fee), 0)
  }

  // Выводим результат
  // Не понятно, число конкретно чего мы пытаемся тут посчитать?
  // console.log('Count transactions', transactions.length)
  console.log('Bakers fees', bakers_fees)
  console.log('Memory (heapUsed, MB)', Math.round(process.memoryUsage().heapUsed / 1024 / 1024 * 100) / 100)
  console.log('Time (seconds)', time())
})()
